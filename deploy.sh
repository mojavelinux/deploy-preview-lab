#!/bin/sh

SERVICE_NAME=$CI_ENVIRONMENT_SLUG
IMAGE_NAME=$CI_REGISTRY/$CI_PROJECT_PATH/nginx:$CI_ENVIRONMENT_SLUG
EXISTS=$(kn service list -o json | jq -r ".items[0].metadata.name == \"$SERVICE_NAME\"")

if [ $EXISTS == "true" ]; then
  kn service delete $SERVICE_NAME
fi

echo "apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: $SERVICE_NAME
  namespace: opendevise-oss-dev
  labels:
    app.kubernetes.io/part-of: nginx
    app.openshift.io/runtime: nginx
    app.openshift.io/runtime-namespace: opendevise-oss-dev
spec:
  template:
    metadata:
      labels:
        app.kubernetes.io/part-of: nginx
        app.openshift.io/runtime: nginx
        app.openshift.io/runtime-namespace: opendevise-oss-dev
    spec:
      containers:
      - image: $IMAGE_NAME
        name: $SERVICE_NAME
        imagePullPolicy: Always
        ports:
        - containerPort: 8080
          protocol: TCP" > service-nginx.yml

oc apply -f service-nginx.yml
