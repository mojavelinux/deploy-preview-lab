FROM nginx:stable-alpine

RUN chmod g+w /var/cache/nginx /run && addgroup nginx root && rm /usr/share/nginx/html/*

COPY ./nginx/ /etc/nginx/
COPY ./public/ /usr/share/nginx/html/

EXPOSE 8080

USER nginx
